#!/usr/bin/python

import random
import math
import numpy as np
import matplotlib
import pylab
from scipy import stats

vary = 10 #for random point generation
dist_cutoff = 5 #distance cutoff

points = []
density = []
nearestNeighborOfHigherDensity = []
numpoints = 0

def rndPoints(x,y,count):
	ps  = []
	for i in range(count):
		p = ( x + random.normalvariate(0.0,5.0), y + random.normalvariate(0.0,5.0))
		ps.append( p )
	return ps

def getSamplePoints():
	p1 = rndPoints(10.0,10.0,200)
	p2 = rndPoints(25.0,25.0,200)
	p3 = rndPoints(40.0,40.0,200)
	return  p1 + p2 + p3

def findDensity(pindx):
	distances = dist_mat[pindx]
	density = 0.0
	for p in range(numpoints):
		if (p != pindx) and (distances[p] < dist_cutoff):
			density += 1.0
	return density

def findMinDistToHigherDensityNeighbor(pindx,max_dist):
	mydensity = density[pindx]
	minDist = float(max_dist)
	for candidate in range(numpoints):
		if (candidate != pindx):
			candidate_dist = dist_mat[pindx][candidate]
			if (candidate_dist < minDist) and (density[candidate] > mydensity):
				minDist = float(candidate_dist)
				nearestNeighborOfHigherDensity[pindx] = candidate
	return minDist

def calculateDistanceMatrix():
	d = {}
	for i in range(numpoints):
		for j in range(numpoints):
			p = (min(i,j),max(i,j))
			d[p] = p

	for (i,j) in d.keys():
		(x1,y1)  = points[i]
		(x2,y2)  = points[j]
		d = math.sqrt (pow(x2-x1,2) + pow(y2-y1,2) )
		dist_mat[i][j] = d
		dist_mat[j][i] = d


def findClusterCenters():
	cutoff = 3.5
	zs = stats.zscore(minDistHigherDensity)
	centers = []
	for indx in range(numpoints):
		if (zs[indx] > cutoff):
			centers.append(indx)
	return centers

def findPointClusterDesignation(point, neighbors, designation):
	c = designation[point]
	if (c == None):
		nn = neighbors[point]
		d = findPointClusterDesignation(nn,neighbors,designation)
		designation[point] = d
		return d
	else:
		return c

def markCenters(cs,designation):
	for c in cs:
		designation[c] = c
	return

def getPointsInCluster(c,desig):
	ps = []
	for i in range(numpoints):
		if (desig[i] == c):
			ps.append(i)
	return ps

def getPlotPoints(ps):
	xp =  []
	yp = []
	for (x,y) in ps:
		xp.append(x)
		yp.append(y)
	return (xp,yp)

def doplot(centers,designation):
	colors = [ "gray","red","blue","yellow","green",
		"gray","red","blue","yellow","green" ]

	color_indx = 0
	for c in centers:
		cpoints = getPointsInCluster(c,designation)
		xp,yp = getPlotPoints(map( lambda p: points[p], cpoints))
		matplotlib.pyplot.scatter(xp,yp, c=colors[color_indx])
		color_indx += 1


	#center points
	#xc,yc  = getPlotPoints(centers)
	#matplotlib.pyplot.scatter(xc,yc, c="red", marker='<')

	matplotlib.pyplot.show()
	
###############################################################
#
# Clustering algo based on work:
# http://people.sissa.it/~laio/Research/Res_clustering.php
# Alex Rodriguez and Alessandro Laio
#
###############################################################


points = getSamplePoints()
numpoints = len(points)
minDistHigherDensity =  np.zeros(numpoints)

density = np.zeros(numpoints)

dist_mat = np.arange( numpoints * numpoints ).reshape(numpoints,numpoints)
calculateDistanceMatrix()

cluster_designation = np.array( [None] * numpoints )
nearestNeighborOfHigherDensity = np.array( [None]*numpoints )
max_dist = max (dist_mat[0])

for indx in range(numpoints):
	density[indx] = findDensity(indx)

for indx in range(numpoints):
	minDistHigherDensity[indx] = findMinDistToHigherDensityNeighbor(indx,max_dist)

centers = findClusterCenters()

markCenters(centers,cluster_designation)

for p in range(numpoints):
	findPointClusterDesignation(p,nearestNeighborOfHigherDensity,cluster_designation)

print "centers", centers

doplot( centers, cluster_designation )



