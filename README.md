# densityclustering

Cluster analysis is a notoriously hard problem. The tried and true k-means algorithm has many drawbacks. But perhaps the most onerous problem with k-means is that it requires you to choose the k (number of clusters). If you don't know how best to classify your data, then its also likely that you don't know how many groups to put it in. Which is one reason I find density clustering so exciting: it automatically determines how many clusters to divide your data points into. If you want to know more about it, you should read the paper, its really quite good.

This is a quickie python implementation of ["Clustering by fast search-and-find of density peaks"](http://people.sissa.it/~laio/Research/Res_clustering.php)

I'm sure there's lots of things wrong with it, so consider this implementation to be "just for fun"

Also, with large sets, you'll run out of memory, so you'll just have to wait till I port this to Apache Spark

![cluster diagram](https://raw.githubusercontent.com/ludflu/densityclustering/master/cluster.png)

In the diagram above, all the points were randomly perturbed from three midpoints. Those random data points were then fed into clustering algorithm. the The colors represent the cluster membership assigned by the density clustering algorithm.
